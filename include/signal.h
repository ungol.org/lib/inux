#ifndef __SIGNAL_H__
#define __SIGNAL_H__

/*
UNG On Linux

MIT License

Copyright (c) 2020 Jakob Kaivo <jkk@ungol.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#if defined _XOPEN_SOURCE && !defined _POSIX_C_SOURCE
#	if (_XOPEN_SOURCE >= 700)
#		define _POSIX_C_SOURCE 200809L
#	elif (_XOPEN_SOURCE >= 600)
#		define _POSIX_C_SOURCE 200112L
#	elif (_XOPEN_SOURCE >= 500)
#		define _POSIX_C_SOURCE 199506L
#	else
#		define _POSIX_C_SOURCE 2
#	endif
#endif

#if defined _POSIX_C_SOURCE && !defined _POSIX_SOURCE
#	define _POSIX_SOURCE
#endif

#include <linux/signal.h>

/* ./src/signal/sig_atomic_t.c */
#ifndef __TYPE_sig_atomic_t_DEFINED__
#define __TYPE_sig_atomic_t_DEFINED__
typedef volatile int                                               sig_atomic_t;
#endif

#if	(defined _XOPEN_SOURCE)
/* src/sys/types/pid_t.c */
#ifndef __TYPE_pid_t_DEFINED__
#define __TYPE_pid_t_DEFINED__
typedef long int                                                          pid_t;
#endif

#endif


#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE))
/* src/ucontext/ucontext_t.c */
#ifndef __TYPE_ucontext_t_DEFINED__
#define __TYPE_ucontext_t_DEFINED__
typedef struct ucontext_t {
	struct ucontext_t * uc_link;
	sigset_t uc_sigmask;
	stack_t uc_stack;
	mcontext_t uc_mcontext;
} ucontext_t;
#endif

#endif


#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE) && _XOPEN_SOURCE < 700)
/* ./src/signal/struct_sigstack.c */
#ifndef __TYPE_struct_sigstack_DEFINED__
#define __TYPE_struct_sigstack_DEFINED__
struct sigstack {
	int ss_onstack;
	void *ss_sp;
};
#endif

#endif

#if (!defined __STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
#define restrict
#endif

int raise(int __sig);
void (*signal(int __sig, void (*__func)(int)))(int);

#if	(defined _POSIX_SOURCE)
int kill(pid_t __pid, int __sig);
int sigaction(int __sig, const struct sigaction * restrict __act, struct sigaction * restrict __oact);
int sigaddset(sigset_t * __set, int __signo);
int sigdelset(sigset_t * __set, int __signo);
int sigemptyset(sigset_t * __set);
int sigfillset(sigset_t * __set);
int sigismember(const sigset_t * __set, int __signo);
int sigpending(sigset_t * __set);
int sigprocmask(int __how, const sigset_t * restrict __set, sigset_t * restrict __oset);
int sigsuspend(const sigset_t * __sigmask);
#endif

#if	(defined _POSIX_C_SOURCE && 199309 <= _POSIX_C_SOURCE)
int sigqueue(pid_t __pid, int __signo, const union sigval __value);
int sigtimedwait(const sigset_t * restrict __set, siginfo_t * restrict __info, const struct timespec * restrict __timeout);
int sigwaitinfo(const sigset_t * restrict __set, siginfo_t * restrict __info);
#endif

#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE))
int killpg(pid_t __pgrp, int __sig);
int sigaltstack(const stack_t * restrict __ss, stack_t * restrict __oss);
int sighold(int __sig);
int sigignore(int __sig);
int siginterrupt(int __sig, int __flag);
int sigpause(int __sig);
int sigrelse(int __sig);
void (*sigset(int __sig, void (*__disp)(int)))(int);
#endif

#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE) && _XOPEN_SOURCE < 500)
int sigmask(int __signum);
#endif

#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE) && _XOPEN_SOURCE < 600)
int sigstack(struct sigstack *__ss, struct sigstack *__oss);
#endif

#if	(defined _XOPEN_SOURCE && ((defined _XOPEN_SOURCE_EXTENDED && _XOPEN_SOURCE_EXTENDED == 1) || 500 <= _XOPEN_SOURCE) && _XOPEN_SOURCE < 700)
void (*bsd_signal(int __sig, void (*__func)(int)))(int);
#endif


#endif
